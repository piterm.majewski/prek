package pl.pmajewski.pretius;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.pmajewski.pretius.nbp.NBPCurrencyRepository;
import pl.pmajewski.pretius.nbp.dto.NBPTable;
import pl.pmajewski.pretius.repository.CurrencyStorage;

import java.math.BigDecimal;

@Slf4j
@Component
@AllArgsConstructor
public class CurrencyStorageLoader {

    private NBPCurrencyRepository nbpCurrencyRepository;
    private CurrencyStorage storage;

    @Scheduled(cron = "0 1 0 * * *") // Run 0:01
    @EventListener(ApplicationReadyEvent.class)
    public void fillWithNBPData() {
        NBPTable table = nbpCurrencyRepository.loadTableA();

        table.getRates().forEach(i -> storage.put(i.getCode(), new BigDecimal(i.getValue())));
        log.info("Storage filled with data...");
    }

}
