package pl.pmajewski.pretius;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.pmajewski.pretius.exception.CurrencyRateNotFoundException;
import pl.pmajewski.pretius.repository.CurrencyStorage;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Service
@AllArgsConstructor
public class ExchangeService {

    private CurrencyStorage storage;

    public List<String> availableCurrencies() {
        return storage.availableCurrencies();
    }

    public BigDecimal computeExchangedValue(String value, String currencyCode, String targetCurrencyCode) {
        BigDecimal valueBD = new BigDecimal(value);
        return computeExchangedValue(valueBD, currencyCode, targetCurrencyCode).setScale(2, RoundingMode.HALF_UP);
    }

    private BigDecimal computeExchangedValue(BigDecimal valueBD, String currencyCode, String targetCurrencyCode) {
        BigDecimal targetRate = storage.get(targetCurrencyCode).orElseThrow(() -> new CurrencyRateNotFoundException());

        if(isValueCodeSameAsStorageBase(currencyCode)) {
            return valueBD.divide(targetRate, 6, RoundingMode.HALF_UP);
        } else if(isTargetCodeSameAsStorage(targetCurrencyCode)) {
            BigDecimal valueRate = storage.get(currencyCode).orElseThrow(() -> new CurrencyRateNotFoundException());
            return valueBD.multiply(valueRate);
        } else {
            BigDecimal intermediateValue = computeExchangedValue(valueBD, currencyCode, storage.baseCurrencyCode());
            return computeExchangedValue(intermediateValue, storage.baseCurrencyCode(), targetCurrencyCode);
        }
    }

    private boolean isTargetCodeSameAsStorage(String targetCurrencyCode) {
        return targetCurrencyCode.equals(storage.baseCurrencyCode());
    }

    private boolean isValueCodeSameAsStorageBase(String currencyCode) {
        return isTargetCodeSameAsStorage(currencyCode);
    }
}
