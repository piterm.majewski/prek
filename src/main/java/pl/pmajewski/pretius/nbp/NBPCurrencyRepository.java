package pl.pmajewski.pretius.nbp;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pl.pmajewski.pretius.nbp.dto.NBPTable;

@Component
@AllArgsConstructor
public class NBPCurrencyRepository {

    private static final String URL = "http://api.nbp.pl/api/exchangerates/tables/A/";

    private RestTemplate restTemplate;

    public NBPTable loadTableA() {
        ResponseEntity<NBPTable[]> response = restTemplate.getForEntity(URL, NBPTable[].class);
        return response.getBody()[0];
    }
}
