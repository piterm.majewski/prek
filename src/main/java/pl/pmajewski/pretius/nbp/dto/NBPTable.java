package pl.pmajewski.pretius.nbp.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class NBPTable {

    private String table;
    private String no;
    private String effectiveDate;
    private List<NBPRate> rates = new ArrayList<>();
}
