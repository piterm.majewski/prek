package pl.pmajewski.pretius.nbp.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

@Data
public class NBPRate {

    private String currency;
    private String code;

    @JsonAlias("mid")
    private String value;
}
