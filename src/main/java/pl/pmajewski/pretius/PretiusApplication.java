package pl.pmajewski.pretius;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PretiusApplication {

    public static void main(String[] args) {
        SpringApplication.run(PretiusApplication.class, args);
    }

}
