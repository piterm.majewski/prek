package pl.pmajewski.pretius;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;

@RestController
@AllArgsConstructor
public class ExchangeController {

    private ExchangeService exchangeService;

    @GetMapping("currencies")
    public List<String> availableCurrencies() {
        return exchangeService.availableCurrencies();
    }

    @GetMapping("exchange")
    public BigDecimal computeExchangedValue(@RequestParam String value,
                                            @RequestParam String currencyCode, @RequestParam String targetCurrencyCode) {
        return exchangeService.computeExchangedValue(value, currencyCode, targetCurrencyCode);
    }
}
