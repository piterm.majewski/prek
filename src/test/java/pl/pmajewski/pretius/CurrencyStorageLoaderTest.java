package pl.pmajewski.pretius;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import pl.pmajewski.pretius.nbp.NBPCurrencyRepository;
import pl.pmajewski.pretius.nbp.dto.NBPRate;
import pl.pmajewski.pretius.nbp.dto.NBPTable;
import pl.pmajewski.pretius.repository.CurrencyStorage;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {CurrencyStorageLoaderTest.TestConfig.class})
@ExtendWith(SpringExtension.class)
class CurrencyStorageLoaderTest {

    public static final String USD_CODE = "USD";

    @Autowired
    private CurrencyStorage storage;

    @Autowired
    private CurrencyStorageLoader loader;

    @BeforeEach
    void init() {

    }

    @Test
    @DirtiesContext
    void shouldLoadNBPDataAfterStart() {
        Optional<BigDecimal> usdValue = storage.get(USD_CODE);
        loader.fillWithNBPData();

        Assertions.assertThat(usdValue.get()).isEqualTo(new BigDecimal("3.6322"));
    }


    /**
     * Configuration loading repository bean before ApplicationReadyEvent
     * {@link CurrencyStorageLoader#fillWithNBPData()}
     */
    @TestConfiguration
    public static class TestConfig {

        @Bean
        @Primary
        public NBPCurrencyRepository mockSystemTypeDetector() {
            NBPCurrencyRepository currencyRepository = mock(NBPCurrencyRepository.class);
            when(currencyRepository.loadTableA()).thenReturn(createDummyTable());
            return currencyRepository;
        }

        public static NBPTable createDummyTable() {
            List<NBPRate> rates = new LinkedList<>();
            rates.add(createRate("dolar amerykański", "USD", "3.6322"));

            NBPTable table = new NBPTable();
            table.setTable("A");
            table.setNo("050/B/NBP/2020");
            table.setEffectiveDate("2020-12-16");
            table.setRates(rates);

            return table;
        }

        public static NBPRate createRate(String currency, String code, String value) {
            NBPRate rate = new NBPRate();

            rate.setCurrency(currency);
            rate.setCode(code);
            rate.setValue(value);

            return rate;
        }
    }
}